let obj = require('../models');
let auth=require('../utils/auth');
let message =obj.message;
let Users =obj.Users;
let Comment =obj.Comment;

let index_fn = async (ctx, next) => {
    let username=ctx.session.userName;

    // 查找所有的说说
    let msgs=JSON.parse(JSON.stringify(await message.findAll({
        order:[
            ["id","desc"]
        ]
    })));
    // 查找所有的评论
    let comts=JSON.parse(JSON.stringify(await Comment.findAll()));

    let userIds=[];


    // 谁对谁说功能 第一步查找到msg中所有的fromUserId
    msgs.forEach(row=>{
        userIds.push(row.fromUserId);
    });

    //谁对谁说功能 第二步查找到comts中所有的fromUserId
    comts.forEach(row=>{
        userIds.push(row.fromUserId);
    });

    //谁对谁说功能 第三步查根据找查到的用户Id，查找用户表中的用户信息

    let userAll=await Users.findAll({
        where:{
            id:userIds
        }
    })

    // 

console.log("userAll:"+JSON.stringify(userAll));

    comts.forEach(row=>{
        let currentUser=userAll.filter(user=>{
            return row.fromUserId===userAll[0].id;
        });
        console.log("userAll.id:"+userAll[0].id)
        console.log("row.fromUserId:"+row.fromUserId)
        console.log("comts:"+comts);
        console.log("username:"+username);
        console.log(currentUser);
        row.comUserName=currentUser.username;
    });

    // 将说说和评论根据msgId按需要组合，将相应的评论挂载在对应的说说上
    msgs.forEach(msg=>{
        let msgComment=comts.filter(item=>{
            return item.msgId===msg.id;
        });
        let currentUser=userAll.filter(row=>{
            return row.id===msg.fromUserId;
        })[0];
        msg.msgUserName=currentUser.username;
        msg.comments=msgComment;
    });


    let obj={
        username:username,
        msgs:msgs
    };
    ctx.render('index.html',obj);
}
let login_fn = async (ctx, next) => {
    ctx.render('login.html');
}
let register_fn = async (ctx, next) => {
    ctx.render('register.html')
}

let loginDone_fn = async (ctx, next) => {
    let username = ctx.request.body.username || '';
    let password = ctx.request.body.password || '';

    console.log(username);
    console.log(password);
    let res;
    let u1=await obj.Users.findOne({where:{
        username:username,
        password:password
    }}).then((row)=>{
        let users=JSON.stringify(row);
        let u2=JSON.parse(users);
        console.log(users);
        console.log(u2);
        
        if(users!=='null'){
            ctx.session.userId=row.id;
            ctx.session.userName=row.username;
            res={code:200,msg:'登录成功'};
        }else{
            res={code:1000,msg:'用户名或密码错误，请重试'};
        }
    });
    ctx.body=res;
}
let registerDone_fn = async (ctx, next) => {
    let username = ctx.request.body.username || '';
    let password = ctx.request.body.password || '';
    let confirmpassword = ctx.request.body.confirmpassword || '';

    console.log(username);
    console.log(password);
    console.log(confirmpassword);

    if (username.length > 0 && password.length > 0 && confirmpassword.length > 0 && password === confirmpassword) {
        let userDemo=await obj.Users.findOne({ where: { username: username } });
        console.log(JSON.stringify(userDemo));
        let res='';

        // 如果有找到同名的用户，则返回提示消息，并且不创建用户；否则创建用户，并返回提示消息
        if(userDemo){
            // msg='当前用户名已经注册，请确认后重试';
            res={code:1000,msg:'当前用户名已经注册，请确认后重试'};
        }else{
            let u1=obj.Users.create({
                username: username,
                password: password
            });
            res={code:200,msg:'注册成功'}; 
        }

        ctx.body=JSON.stringify(res);

    } else {
        console.log('用户名或密码不能为空；并且两次密码应该相同');
    }
}

//注销

let jump_fn = async (ctx,next)=>{
    ctx.session=null;
    ctx.redirect('/login');
}



let say_fn=async (ctx,next)=>{
    let msgSay=ctx.request.body.msgSay || '';
    let userId=ctx.session.userId;


    console.log("说说内容："+msgSay);
    console.log("userId："+userId);
    let msg1=await message.create({
        fromUserId:userId,
        content:msgSay
    });



    let data=JSON.stringify(msg1)


    console.log(msgSay);

    ctx.body={code:200,data,msg:'发表成功'};
}
let comment_fn=async (ctx,next)=>{
    let msgId=ctx.request.body.msgId || '';
    let userId=ctx.session.userId;
    let commentText=ctx.request.body.content || '';
    let msg1=await Comment.create({
        fromUserId:userId,
        msgId:msgId,
        content:commentText
    });

    // 根据fromUsreId查询对应的用户名
    let fromUser=await obj.Users.findOne({
        where:{
            id:userId
        }
    });

    //根据msgId找到fromUserId
    let msg=await obj.message.findOne({
        where:{
            id:msgId
        }
    });

    //根据找到fromUserId，查找相对应的用户名
    let toUser=await obj.Users.findOne({
        where:{
            id:msg.fromUserId
        }
    });

    let data=JSON.parse(JSON.stringify(msg1));

    data.fromUserName=fromUser.username;
    data.toUserName=toUser.username;

    ctx.body={code:200,data,msg:'发表成功'};
}

module.exports = {
    '/': ['get',auth, index_fn],
    '/login': ['get',auth, login_fn],
    '/register': ['get', register_fn],
    '/loginDone': ['post', loginDone_fn],
    '/registerDone': ['post', registerDone_fn],
    '/say': ['post', say_fn],
    '/comment': ['post', comment_fn],
    '/jump':['get',jump_fn]
}
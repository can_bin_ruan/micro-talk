// 1、遍历controllers目录，找出所有的js文件
let fs = require('fs');
let path = require('path');
let router = require('koa-router')();

// 寻找指定路径下的路由文件
function searchControllers(defaultDir){
    // 利用文件模块，读取控制器目录下的所有文件
    let files = fs.readdirSync(defaultDir);
    // 筛选真实的路径文件
    let resultFiles = files.filter((fileName) => {
        return fileName.endsWith('.js') && fileName !== 'index.js';
    });

    return resultFiles;
}

// 注册给定的路由数组下的所有定义的路由
function registeForEachFile(files,currentPath){
    //遍历每个路径文件
    files.forEach(item => {
        let tmpPath = path.join(currentPath, item);
        // 引入路径文件，准备遍历其暴露的路径定义
        let routerObj = require(tmpPath);
        // 循环定义路由及对应的处理函数
        for (let r in routerObj) {
            let arrValue=routerObj[r];
            // 获取路由定义的类型，是get请求还post请求，分别调用不同的函数来调用
            let type = routerObj[r][0];
            // 获取路由定义中的请求后要调用的函数
            // let fn = routerObj[r][1];
            if (type === 'get') {
                // 为了判断传入的数组里面有没有包含auth，没有正按正常处理；有的话，需要auth传入作为第二参数
                if(arrValue.length===2){
                    let fn=arrValue[1];
                    router.get(r, fn);
                }else{
                    let auth=arrValue[1];
                    let fn=arrValue[2];
                    router.get(r,auth,fn);
                }                
            } else {
                // 为了判断传入的数组里面有没有包含auth，没有正按正常处理；有的话，需要auth传入作为第二参数
                if(arrValue.length===2){
                    let fn=arrValue[1];
                    router.post(r, fn);
                }else{
                    let auth=arrValue[1];
                    let fn=arrValue[2];
                    router.post(r,auth,fn);
                }
            }
        }
    })
}


// 3、将本文件当作控制器目录的入口，在app.js当中作为中间注册

module.exports = function(dir){
    let defaultDir=dir || '/controllers';
    // 项目当前绝对路径
    let root = path.resolve('.');
    // 获得控制器的绝对路径
    let resultControllerPath = path.join(root, defaultDir);

    // 获得控制器目录下的所有路由文件
    let files=searchControllers(resultControllerPath);

    // 注册定义的路由
    registeForEachFile(files,resultControllerPath);

    //返回待在app执行的注册函数
    return router.routes();
};